var crypto = require('crypto');
const secretToken = 'tuanva';

function regenerateAPIKEY(domainName) {
    return crypto.createHash('md5').update((domainName + secretToken)).digest("hex");
}

function checkAPIKEY(token, domainName) {
    if (token == regenerateAPIKEY(domainName)) {
        return true;
    }
    return false;
}

exports.regenerateAPIKEY = regenerateAPIKEY;
exports.checkAPIKEY = checkAPIKEY;