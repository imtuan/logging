'use strict';
const Joi = require('joi');
const loggingController = require('./controllers/logging');
const domainController = require('./controllers/domain');
const userController = require('./controllers/user');
const helper = require('./helper');
const Inert = require('inert');

module.exports = [
    {
        method: 'POST', path: '/log/event', handler: loggingController.create,
        config: {
            validate: {
                params: function(val, options, next) {
                    const domainx = options.context.headers.origin;
                    var regex = /(?:[\w-]+\.)+[\w-]+/
                    const domainName = regex.exec(domainx)[0];
                    const token = options.context.query.token;
                    if (helper.checkAPIKEY(token, domainName)) {
                        next(null, val);
                    } else {
                        var err = new Error("Token not valid");
                        next(err, val);
                    }
                },
                payload: {
                    eventId: Joi.string(),
                    data: Joi.any(),
                },
            },
        },
    },
    {
        method: ['POST'], path: '/export/{eventId}', handler: loggingController.export,
        config: {
            validate: {
                payload: {
                    template: Joi.string(),
                    query: Joi.string(),
                },
                params: {
                    eventId: Joi.string().required(),
                }
            },
        },
    },
    {
        method: ['GET'], path: '/export/{eventId}', handler: loggingController.download,
        config: {
            validate: {
                params: {
                    eventId: Joi.string().required(),
                },
                query: {
                    download: Joi.string()
                }
            },
        },
    },
    {
        method: 'POST', path: '/domain', handler: domainController.add,
        config: {
            validate: {
                payload: {
                    domain: Joi.string().required(),
                    token: Joi.string().required(),
                },
            }
        }
    },
    {
        method: 'GET', path: '/domain/{domain?}', handler: domainController.get,
        config: {
            validate: {
                params: {
                    domain: Joi.string(),
                },
                query: {
                    token: Joi.string().required(),
                }
            }
        }
    },
    {
        method: 'DELETE', path: '/domain/{domain?}', handler: domainController.remove,
        config: {
            validate: {
                params: {
                    domain: Joi.string(),
                },
            }
        }
    },
    {
        method: 'POST', path: '/user/signup', handler: userController.add,
        config: {
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password: Joi.string().required(),
                }
            }
        }
    },
    {
        method: 'POST', path: '/user/login', handler: userController.login,
        config: {
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password: Joi.string().required(),
                }
            }
        }
    },
    {
        method: 'GET', path: '/download/{file}', handler: (request, reply) => {
            const filename = request.params.file;
            return reply.file('./log_export/' + filename);
        },
        config: {
            validate: {
                params: {
                    file: Joi.string().required(),
                },
            },
        },
    },
];