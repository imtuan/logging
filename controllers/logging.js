'use strict';
const Boom = require('boom');
const Logging = require('../models/logging');
const request = require('request');
const fs = require("fs");

function createLog(data, callback) {
    Logging.create(data, function (err, doc) {
        if (err) callback(Boom.badImplementation());
        else return callback(null, doc);
    });
}

function create(request, reply) {
    const postData = request.payload;
    const data = {
        eventId: postData.eventId,
        data: postData.data,
    }

    createLog(data, (err, message) => {
        if (err) {
            return reply(Boom.badData(err));
        }
        reply(message)
    });
}

function exportData(request, reply) {
    const eventId = request.params.eventId;
    let queryCondition = '';
    if (request.payload.query != undefined) {
        queryCondition = request.payload.query;
    }

    let condition = '';
    if (queryCondition == '') {
        condition = { eventId: eventId };
    } else {
        const dataQuery = JSON.parse(queryCondition);
        condition = { eventId: eventId, data: dataQuery }
    }

    Logging.find(condition, (err, logs) => {
        return reply(logs);
    })
}

function download(request, reply) {
    const eventId = request.params.eventId;
    let download = false;
    if (request.query.download != undefined) {
        download = true;
    };

    let condition = { eventId: eventId };

    Logging.find(condition, (err, logs) => {
        if (download == true) {
            const fileName = "./log_export/" + eventId + "_" + Date.now() + ".json";
            fs.writeFile(fileName, JSON.stringify(logs), "utf8", function () {
                // return reply('success!');
                return reply.file(filename);
            });
        } else {
            return reply(logs);
        }
    })
}

exports.create = create;
exports.export = exportData;
exports.download = download;