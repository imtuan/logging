'use strict';
const Boom = require('boom');
const request = require('request');
const domainModel = require('../models/domain');
const helper = require('../helper');

function add(request, reply) {
    const domainName = request.payload.domain;
    const userId = request.payload.token;
    domainModel.findOne({ domain: domainName }, (err, domain) => {
        if (err) {
            return reply(Boom.badImplementation());
        }
        if (domain) {
            return reply(Boom.badData('This domain exist!'));
        }
    });

    domainModel.create({ domain: domainName, userId: userId }, (err, doc) => {
        if (err) {
            return reply(Boom.badImplementation());
        } else {
            return reply({ status: true, message: 'Success!' });
        }
    });
}

function get(request, reply) {
    const userId = request.query.token;
    let condition = { userId: userId };

    if (request.params.domain) {
        condition['domain'] = request.params.domain
    }

    domainModel.find(condition, (err, doc) => {
        if (err) {
            return eply(Boom.badImplementation());
        }
        if (!doc) {
            return reply(Boom.badData('Domain not find'));
        }
        if (doc) {
            // xxx['API_TOKEN'] = helper.regenerateAPIKEY(domainName['domain']);
            return reply(doc);
        }
    })
}

function remove(request, reply) {
    const domainId = request.params.domain;

    domainModel.remove({ _id: domainId }, (err) => {
        if (err) {
            return reply(Boom.badImplementation());
        }
        return reply({ status: true });
    })
}


exports.add = add;
exports.get = get;
exports.remove = remove;