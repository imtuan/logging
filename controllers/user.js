'use strict';
const Boom = require('boom');
const userModel = require('../models/user');
const helper = require('../helper');

function add(request, reply) {
    const email = request.payload.email;
    const password = request.payload.password;
    userModel.findOne({ email: email }, (err, doc) => {
        if (doc) {
            return reply(Boom.badData('Email đã tồn tại'));
        }
        if (err) {
            return reply(Boom.badImplementation());
        }
        userModel.create({ email: email, password: password }, (err, doc) => {
            if (doc) {
                return reply({ status: true });
            } else {
                return reply(Boom.badImplementation());
            }
        });
    })
}

function get(request, reply) {
}

function login(request, reply) {
    const email = request.payload.email;
    const password = request.payload.password;
    userModel.findOne({ email: email, password: password }, (err, doc) => {
        if (err) {
            return reply(Boom.badImplementation);
        }
        if (doc.length == 0) {
            return reply(Boom.badData('Login detail incorect!'));
        }
        return reply({ status: true, token: doc._id });
    })
}


exports.add = add;
exports.get = get;
exports.login = login;