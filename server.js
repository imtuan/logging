'use strict';

const Hapi = require('hapi');
const routers = require('./routes');
const mongoose = require('mongoose');

// Create a server with a host and port
const server = new Hapi.Server();

server.connection({ port: process.env.PORT || 8080, routes: { cors: true } });
server.route(routers);


// Start the server
server.register(require('inert'), (err) => {
    server.start((err) => {
        if (err) {
            throw err;
        }

        mongoose.Promise = global.Promise;
        mongoose.connect('mongodb://bastian:bastian@cluster0-shard-00-00-wp4vq.mongodb.net:27017,cluster0-shard-00-01-wp4vq.mongodb.net:27017,cluster0-shard-00-02-wp4vq.mongodb.net:27017/53?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin')
            .then(() => console.log('connection succesful'))
            .catch((err) => console.error(err));

        console.log('Server running at:', server.info.uri);
    });
});