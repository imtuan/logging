const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const schema = new Schema({
	eventId: { type: String, required: true },
	data: Schema.Types.Mixed,
},
	{
		timestamps: true
	});

var logging = mongoose.model('logging', schema);

module.exports = logging;