const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const schema = new Schema({
    domain: { type: String, required: true, unique: true },
    userId: { type: String, required: true }
},
    {
        timestamps: true
    });

var domainName = mongoose.model('domain', schema);

module.exports = domainName;