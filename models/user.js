const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create a schema
const schema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
},
    {
        timestamps: true
    });

var user = mongoose.model('user', schema);

module.exports = user;