var LogCore = function (APIKEY) {
    this.URL = 'http://log.pasger.com/log/event?token=' + APIKEY;
    this.httpPOST = function (eventKey, data, callback = null) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', this.URL, true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        xhr.send(JSON.stringify({ eventId: eventKey, data: data }));
        xhr.onloadend = function () {
            if (callback != null) {
                callback();
            }
        };
    };

    this.sendLog = function (eventId, data, callback = null) {
        this.httpPOST(eventId, data, callback);
    }
}